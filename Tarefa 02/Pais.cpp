#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){
    
    int i, temp=1;  
    pid_t pid_neto_1,pid_neto_2;
    
    
    pid_neto_1 = fork();
    
    switch(pid_neto_1){
        case -1:
            exit(1);
            break;
        case 0:
            printf("------------------>Filho do Pai 1 (Neto 1) Criado\n");
            
            temp = 10*(getpid() - getppid());

            printf("------------------>Neto 1 - Vou dormir por %d segundos\n",temp);
            sleep(temp);
                        
                        
            printf("------------------> Neto 1- terminando sua execucao!\n" );            
            exit(1);
            break;
        default:
            pid_neto_2 = fork();
            switch(pid_neto_2){
                case -1:
                    exit(1);
                    break;
                case 0:
                    
                    printf("------------------>Filho do Pai 1 (Neto 2) Criado\n");
                    
                    temp = 10*(getpid() - getppid());

                    printf("------------------>Neto 2 - Vou dormir por %d segundos\n",temp);
                    sleep(temp);
                            
                            
                    printf("------------------> Neto 2 - terminando sua execucao!\n" );            
                    exit(1);
                    break;
                default:
                    break;
            }
            break;
    }
    
    temp = 5*(getpid() - getppid());
    
    printf("--->Pai 1 - Vou dormir por %d segundos\n",temp);
    sleep(temp);
    
    printf("--->Pai 1 - Vou esperar meus filhos terminarem a execução\n");
    wait(NULL) ;
    wait(NULL) ;
                        
    printf("--->Pai 1 - terminando sua execucao!\n");
    
    exit(1);
    
}
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(){

printf("\n---------------------------------\n");
    printf("Programa Lista 2....\n\n");

    pid_t pid_pai_1,pid_pai_2;
    
    
    char message[15];
    strcpy(message, "Processo avô");
        
    printf("-----------------------------------------\n");
    printf("%s \n",message);
    printf("-----------------------------------------\n");
    
    pid_pai_1 = fork();
    
    
    
    
    switch(pid_pai_1){
        case -1:
            exit(1);
            break;
        case 0:
            printf("--->Filho do Avô (Pai 1) Criado\n");
            
            execv("Pais", NULL);
            exit(1);
            break;
        default:
            pid_pai_2 = fork();
            switch(pid_pai_2){
                case -1:
                    exit(1);
                    break;
                case 0:
                    printf("--->Filho do Avô (Pai 2) Criado\n");
                    execv("Pais2", NULL);
                    exit(1);
                    break;
                default:
                    break;
            }
            break;
    }

    
    printf("%s - Vou esperar meus filhos terminarem a execução\n",message);
    wait(NULL) ;
    wait(NULL) ;
                        
    printf("%s - terminando sua execucao!\n", message);
    exit(1);
}

    

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

#define LEITORES    5       //quantidade de leitores
#define ESCRITORES    3        //quantidade de escritores



pthread_mutex_t db;         //exclusao mutua para regioes criticas... controla o acesso ao banco de dados
pthread_mutex_t mutex;      //exclusao mutua para regioes cri­ticas... controla o acesso a variável rc
int   rc;                   //quantidade de leitores lendo ou querendo ler a base de dados


// prototipos-------------------------------------

void lendo_base_de_dados(void);
void Usar_base_de_dados(void);
void escritor_pensando(void);

void escrevendo_base_de_dados(void);

void ler() {
    while (1) {  

        pthread_mutex_lock(&mutex);           
        rc=rc+1;                              
        if(rc==1)  pthread_mutex_lock(&db);   
        
        pthread_mutex_unlock(&mutex);       

        lendo_base_de_dados();
        
        
        pthread_mutex_lock(&mutex);           
        rc=rc-1;                             

        if(rc==0) pthread_mutex_unlock(&db);  
        pthread_mutex_unlock(&mutex);         

        Usar_base_de_dados();
    }
}

void escrever() {

    while(1){
        escritor_pensando();    

        pthread_mutex_lock(&db);  

        escrevendo_base_de_dados();     

        pthread_mutex_unlock(&db);  
    }
}

void lendo_base_de_dados()
{

    /*quanto tempo o leitor permanecerá lendo a base de dados*/
    int tempo_leitura;
    tempo_leitura = rand() % 3;

    printf("-----> Leitor utilizando os arquivos do banco de dados. Total de %d leitores utilizando agora.\n",rc);
    sleep(tempo_leitura);
    
}


int main(){

    pthread_t escritor_threads[ESCRITORES], leitor_threads[LEITORES]; 
    int i;

//inicializacao dos semaforos...
    pthread_mutex_init(&db, NULL);
    pthread_mutex_init(&mutex, NULL);


//criação das threads independentes de escritores...
    for(i=0;i<ESCRITORES;i++){
        pthread_create( &escritor_threads[i], NULL,(void *) escrever, NULL);
    }

//criação das threads independentes de leitores...
    for(i=0;i<LEITORES;i++){
        pthread_create( &leitor_threads[i], NULL,(void *) ler, NULL);
    }

 
 
 
 
    for(i=0;i<ESCRITORES;i++){
        pthread_join(escritor_threads[i], NULL);
    }

    for(i=0;i<LEITORES;i++){
        pthread_join(leitor_threads[i], NULL);
    }


    exit(0);
}


void Usar_base_de_dados()
{
    int tempo_uso;
    tempo_uso = rand() % 10;

    printf("-----> Leitor utilizando conhecimento adquirido no banco de dados\n");
    sleep(tempo_uso);
}

void escritor_pensando()
{
    int tempo_pensando;
    tempo_pensando = rand() % 10;

    printf("Escritor pensando no que irá adicionar no banco de dados\n");
    sleep(tempo_pensando);
}

void escrevendo_base_de_dados()
{
    int tempo_escrevendo;
    tempo_escrevendo = rand() % 6;

    printf("Escritor adicinando material no banco de dados\n");
    sleep(tempo_escrevendo);
}


#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>

#define SLEEP_TIME_MAIN         5
#define SLEEP_TIME_FUNCTION     3


int itens;
sem_t mutex;

void* consumidor(void *arg);

int main(){
    printf("------ Problema Produtor/Consumidor ------\n");
    
    
    itens = 0;

    
    int         status;
    pthread_t   threadID;
    void        *thread_return;

    
    status = sem_init(&mutex, 0, 1);
    if (status != 0) {
        perror("Thread MAIN() -> Falha em \"sem_init\"!\n");
        exit(EXIT_FAILURE);
    }  
    
    status = pthread_create(&threadID, NULL, &consumidor, NULL);
    if (status != 0) {
        perror("Thread MAIN() -> Falha em \"pthread_create\".\n");
        exit(EXIT_FAILURE);
    }
    
     while(1){
        if(itens<10){
            printf("Thread Produtor -> Tentando Produzir \"itens\"...\n");
            
            
            status = sem_wait(&mutex);
            if (status != 0){
                printf("Thread Produtor -> Falha em \"sem_wait\".\n");
                exit(EXIT_FAILURE);
            }
                
            
            itens = itens + 1;
            printf("Thread Produtor -> Wait(Mutex).\n");
            printf("Thread Produtor -> Novo valor de \"itens\": %d.\n", itens);
            printf("Thread Produtor -> Indo dormir por %d segundos...\n", SLEEP_TIME_MAIN);
            
            
            sleep(SLEEP_TIME_MAIN);
            
            
            status = sem_post(&mutex);
            if (status != 0){
                printf("Thread Produtor -> Falha em \"sem_post\".\n");
                exit(EXIT_FAILURE);
            }
            
            
            printf("Thread Produtor -> Post(Mutex).\n");
            usleep(1);
         
        }
    }
    
}

void* consumidor(void *arg){

    int status;
    
    
    while(1){
        if(itens>0){
            printf("------> Thread Cunsumidor -> Tentando retirar \"itens\"...\n");
            
            
            status = sem_wait(&mutex);
            if (status != 0){
                printf("Thread Consumidor -> Falha em \"sem_wait\".\n");
                exit(EXIT_FAILURE);
            }
            
            
            itens = itens - 1;
            printf("------> Thread Consumidor -> Wait(Mutex).\n");
            printf("------> Thread Consumidor -> Novo valor de \"itens\": %d.\n", itens);
            printf("------> Thread Consumidor -> Indo dormir por %d segundos...\n", SLEEP_TIME_FUNCTION);
            
            
            sleep(SLEEP_TIME_FUNCTION);
            
            
            status = sem_post(&mutex);
            if (status != 0){
                printf("Thread Consumidor -> Falha em \"sem_post\".\n");
                exit(EXIT_FAILURE);
            }
            
            
            printf("------> Thread Consumidor -> Post(Mutex).\n");
            usleep(1);
        }
    }
    
    
    pthread_exit(NULL);
}
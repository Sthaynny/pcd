#include "semaforo.h"
#include <time.h>
#include <sys/shm.h>

#define CHAVE_MEM   1       //Chave que ira identificar a memoria compartilhada a ser criada.
#define TAM_MEM     4096  //Tamanho da memoria compartilhada que sera criada.

#define CHAVE_MEM2   2       //Chave que ira identificar a memoria compartilhada a ser criada.
#define TAM_MEM2     4096  //Tamanho da memoria compartilhada que sera criada.


#define CHAVE_SEM_0   121   //Chave que ira identificar o primeiro semaforo a ser criado.

struct buffer{
    pid_t pid;
    time_t tempo_real;
    clock_t num_clocks;
};



int main(){
    
    int memID;                  //Identificador da memoria compartilhada a ser criada.
    void *memAux = (void *) 0;  //Ponteiro para a memoria compartilhada a ser criada.
    struct buffer *mem;          //Ponteiro para a manipular a memoria compartilhada a ser criada.
    
    memID = shmget((key_t) CHAVE_MEM, TAM_MEM, 0666|IPC_CREAT);    
    if (memID == -1){
        fprintf(stderr, "Shmget da memoria falhou\n");     
        exit(EXIT_FAILURE);	                          
    }

    memAux = shmat(memID, (void *) 0, 0);  
    
    if (memAux == (void *) -1){
        fprintf(stderr, "Shmat da memoria falhou\n");
        exit(EXIT_FAILURE);                                
    }
    
    mem = (struct buffer*) memAux;
    
    int memID2;                  //Identificador da memoria compartilhada a ser criada.
    void *memAux2 = (void *) 0;  //Ponteiro para a memoria compartilhada a ser criada.
    int *count;          //Ponteiro para a manipular a memoria compartilhada a ser criada.
    
    memID2 = shmget((key_t) CHAVE_MEM2, TAM_MEM2, 0666|IPC_CREAT); 
    
    if (memID2 == -1){
        fprintf(stderr, "Shmget da memoria falhou\n");     
        exit(EXIT_FAILURE);	                          
    }

    
    memAux2 = shmat(memID2, (void *) 0, 0);  
    
    if (memAux2 == (void *) -1){
        fprintf(stderr, "Shmat da memoria falhou\n");
        exit(EXIT_FAILURE);                                
    }
    
    count = (int*) memAux2;
    
    Semaforo    S0(CHAVE_SEM_0, 1, 0666|IPC_CREAT);  

    printf("------ Processo Consumidor Iniciado! ------\n");    
   
    while(1){
        S0.down();
        if(*count>0){
            
                *count-=1;
                mem[*count].pid = getpid();
                mem[*count].tempo_real= time((time_t *) 0);
                
                mem[*count].num_clocks = clock(); 
                
                printf("--->Retirada \n");
                printf("pid: %d\n",mem[*count].pid);
                printf("Tempo real: %ld\n",mem[*count].tempo_real);
                printf("Numero de Cloks: %ld\n",mem[*count].num_clocks);
                printf("Posição: %d\n",*count);
                
        }
        S0.up();
        sleep(5);
    
    }
    
}